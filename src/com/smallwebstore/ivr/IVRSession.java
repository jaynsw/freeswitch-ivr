package com.smallwebstore.ivr;

import java.io.Writer;

import org.freeswitch.esl.domain.Event;
import org.freeswitch.esl.domain.EventHeaderNames;
import org.freeswitch.esl.io.Channel;
import org.freeswitch.esl.io.IEngine;
import org.freeswitch.esl.io.ISession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.smallwebstore.dao.IDAOFactory;
import com.smallwebstore.ivr.domain.IService;
import com.smallwebstore.ivr.service.IServiceDAO;

public class IVRSession implements ISession {

	private static final Logger log = LoggerFactory.getLogger(IVRSession.class);

	private IEngine engine;

	private IConfig config;

	private String state = null;

	Pipeline pipeline = null;

	private String sessionId;
	
	private Writer writer;
	
	private Event channelCreated;
	
	private Channel channel;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public IVRSession(IEngine engine, IConfig config, Channel channel, Event event) {
		this.engine = engine;
		this.config = config;
		this.channelCreated = event;
		this.channel = channel;
		init();
	}

	@Override
	public void onEvent(Channel ch, Event event) {

		log.info("---Session onEvent channl {} event {}",
				ch.getChannelId(), event.getEventName());
		
		ISession handler = pipeline.getSession();
		handler.onEvent(ch, event);
		
	}
	
	

	public void init() {
		
			String DNIS = channelCreated
					.getEventHeader(EventHeaderNames.CALLER_DESTINATION_NUMBER);

			IDAOFactory daoFactory = config.getDAOFactory();
			String datasource = config.getDataSource();

			String phoneNumber = DNIS;
			IServiceDAO servicedao = daoFactory.newIntance(IServiceDAO.class,
					datasource);
			IService service = servicedao.getByNumber(phoneNumber);
			if (service == null) {
				channel.hangup(0, 28);
				return;
			}

			
			String formCode = service.getFormCode();

			pipeline = new Pipeline(config, engine, this, service, channel);
			
			pipeline.redirect(formCode);

		
	}

	@Override
	public void setLogger(Writer out) {
		this.writer = out;
	}

	@Override
	public Writer getLogger() {
		return writer;
	}

}

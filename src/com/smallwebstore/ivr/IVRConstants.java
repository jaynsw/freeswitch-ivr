package com.smallwebstore.ivr;

public abstract class IVRConstants {
	public final static String DATASOURCE = "datasource";
	public final static String NULL_PROMPT = "null_prompt";
	public final static String SIP_GATEWAY = "sip_gateway";
	public final static String OUTBOUND_PLAN = "outbound_plan";
	public final static String WAV_ROOT = "wav_root";
}

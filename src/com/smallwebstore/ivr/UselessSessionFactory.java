package com.smallwebstore.ivr;

import org.freeswitch.esl.domain.Event;
import org.freeswitch.esl.io.Channel;
import org.freeswitch.esl.io.IEngine;
import org.freeswitch.esl.io.ISession;
import org.freeswitch.esl.io.ISessionFactory;

public class UselessSessionFactory implements ISessionFactory{

	private IConfig config;
	
	public UselessSessionFactory(IConfig conf){
		this.config = conf;
	}
	
	@Override
	public ISession newSession(IEngine engine, Channel channel, Event event) {
		return new UselessSession(engine, config, channel, event);
	}
	

}

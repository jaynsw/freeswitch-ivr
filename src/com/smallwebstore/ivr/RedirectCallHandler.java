package com.smallwebstore.ivr;

import java.util.TimeZone;

import org.freeswitch.esl.io.Channel;
import org.freeswitch.esl.io.SimpleSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.smallwebstore.dao.IDAOFactory;
import com.smallwebstore.ivr.domain.IEndPoint;
import com.smallwebstore.ivr.domain.IForm;
import com.smallwebstore.ivr.domain.IPoint;
import com.smallwebstore.ivr.domain.IService;
import com.smallwebstore.ivr.domain.ITimeEndPoint;
import com.smallwebstore.ivr.service.IEndPointDAO;
import com.smallwebstore.ivr.service.ITimeEndPointDAO;


public class RedirectCallHandler extends SimpleSession implements IVRHandler{

private static final Logger log = LoggerFactory.getLogger(OptionsFormHandler.class);
	 
	
	private String state = null;
	
	private IForm form = null;
	private IService service = null;
	private IConfig config = null;
	
	
	private IPoint terminationPoint = null;
	
	private Channel ch;
	private Pipeline pipeline;
	
	public RedirectCallHandler(Pipeline pipeline,  IForm form){
		super(pipeline.getEngine());
		this.pipeline = pipeline;
		this.service = pipeline.getService();
		this.form = form;
		this.ch = pipeline.getChannel();
		this.config = pipeline.getConfig();
	}
	
	
	
	
	
	public void run() {
		String website = service.getWebsite();
		String accountCode = service.getAccountCode();
		IDAOFactory daoFactory = config.getDAOFactory();
    	String datasource = config.getDataSource();
		
    	
		
		String timezone = service.getTimeZone();
		
		IEndPointDAO ptdao = daoFactory.newIntance(IEndPointDAO.class, datasource);
		
		ITimeEndPointDAO tptdao = daoFactory.newIntance(ITimeEndPointDAO.class, datasource);
		String dtmf = IPoint.DEFAULT_DTMF;
		IEndPoint ppt = ptdao.getByDTMF(form.getCode(), dtmf);
		if(ppt == null){
			notFound();
			return;
		}
		
		ITimeEndPoint tppt = tptdao.getByEndPointCodeAndTime(ppt.getCode(), TimeZone.getTimeZone(timezone), System.currentTimeMillis());
		if (tppt != null) {
			terminationPoint = tppt;
		} else {
			terminationPoint = ppt;
		}
		
		if (terminationPoint == null){
			notFound();
			return;
		}
		
		pipeline.next(terminationPoint);
		
		
	}
	
	private void notFound(){
		log.warn("redirect form the termination number is null");
		ch.hangup(0, 28);
	}
	
}
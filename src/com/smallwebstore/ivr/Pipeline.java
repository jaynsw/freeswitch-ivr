package com.smallwebstore.ivr;

import org.freeswitch.esl.io.Channel;
import org.freeswitch.esl.io.IEngine;
import org.freeswitch.esl.io.ISession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.smallwebstore.dao.IDAOFactory;
import com.smallwebstore.ivr.domain.IForm;
import com.smallwebstore.ivr.domain.IPoint;
import com.smallwebstore.ivr.domain.IService;
import com.smallwebstore.ivr.service.IFormDAO;

public class Pipeline {
	private static final Logger log = LoggerFactory.getLogger(Pipeline.class);
	
	private IConfig config;
	
	private IService service;
	
	ISession handler = null;
	
	Channel ch = null;
	
	IEngine engine;
	
	private String outboundCallerId;
	
	private ISession session;
	
	
	
	public Pipeline(IConfig config,IEngine engine, ISession session, IService service, Channel channel){
		this.engine = engine;
		this.config = config;
		this.service = service;
		this.ch = channel;
		this.outboundCallerId = channel.getFrom();
		this.session = session;
		this.handler = session;
	}
	
	public ISession getSessoin(){
		return session;
	}
	
	public IEngine getEngine(){
		return engine;
	}
	public IConfig getConfig(){
		return config;
	}
	
	public IService getService(){
		return service;
	}
	
	public Channel getChannel(){
		return ch;
	}
	
	public ISession getSession(){
		return handler;
	}
	
	public String getOutbondCallerId(){
		return outboundCallerId;
	}
	
	
	private void handleNotFound(){
		ch.hangup(0, 28);
	}
	
	void setOutboundCallerId(String val){
		this.outboundCallerId = val;
	}
	public void redirect(String formCode){
		IDAOFactory factory = config.getDAOFactory();
		String datasource = config.getDataSource();
		
		IFormDAO formdao = factory.newIntance(IFormDAO.class, datasource);
		IForm form = formdao.getByCode(formCode);
		if (form != null) {
			RedirectCallHandler obj = new RedirectCallHandler(this, form);
			handler = obj;
			obj.run();
		} else {
			log.warn("redirctcall {} not found!", formCode);
			handleNotFound();
			
		} 
	}
	public void options(String formCode){
		IDAOFactory factory = config.getDAOFactory();
		String datasource = config.getDataSource();
		
		IFormDAO formdao = factory.newIntance(IFormDAO.class, datasource);
		IForm form = formdao.getByCode(formCode);
		if (form != null) {
			OptionsFormHandler obj = new OptionsFormHandler(this, form);
			handler = obj;
			obj.run();
		} else {
			log.warn("options form {} not found!", formCode);
			handleNotFound();
		}
	}
	
	public void outbound(IPoint pt){
		OutboundCallHandler obj = new OutboundCallHandler(this, pt);
		handler = obj;
		obj.run();
	}
	
	public void next(IPoint pt){
		String category = pt.getCategory();
		
		if (IPoint.NUMBER.equals(category)){
			outbound(pt);
		} else if (IPoint.OPTIONS.equals(category)){
			options(pt.getNumber());
		} else if (IPoint.REDIRECT.equals(category)){
			redirect(pt.getNumber()); 
		} else {
			log.warn("category  {} not found!", category);
			handleNotFound();
		}
		
	}
	
	
}

package com.smallwebstore.ivr;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.freeswitch.esl.domain.Command;
import org.freeswitch.esl.domain.CommandResponse;
import org.freeswitch.esl.domain.Event;
import org.freeswitch.esl.domain.EventHeaderNames;
import org.freeswitch.esl.domain.ICommandCallback;
import org.freeswitch.esl.io.Channel;
import org.freeswitch.esl.io.SimpleSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.smallwebstore.ivr.domain.IPoint;
import com.smallwebstore.ivr.domain.IService;

public class OutboundCallHandler extends SimpleSession implements IVRHandler{

	private static final Logger log = LoggerFactory.getLogger(OutboundCallHandler.class);
	 
	
	private String state = null;
	
	private IService service = null;
	private IConfig config = null;
	private Channel ch = null;
	
	private IPoint point = null;
	
	private Pipeline pipeline;
	
	private Channel outbound;
	private Channel inbound;
	
	private String callerId;

	private Iterator<String> nextNumber = null;
	
	public OutboundCallHandler(Pipeline pipeline, IPoint point){
		super(pipeline.getEngine());
		this.pipeline = pipeline;
		this.service = pipeline.getService();
		this.point = point;
		this.ch = pipeline.getChannel();
		this.config = pipeline.getConfig();
		this.callerId = pipeline.getOutbondCallerId();
		String uuid = UUID.randomUUID().toString();
		inbound = pipeline.getChannel();
		this.outbound = new Channel(uuid, pipeline.getEngine(), Channel.OUTBOUND);
		this.outbound.setSession(inbound.getSession());
		pipeline.getEngine().addChannel(outbound);
		
		String nums = point.getNumber();
		
		if (nums != null){
			String[] ns = nums.split(",");
			List<String> numbers = new ArrayList<String>();
			for (String ln : ns){
				numbers.add(ln);
			}
			nextNumber = numbers.iterator();
		}
		
	}
	
	public void run(){
		dialNext();
	}
	
	private void dialNext(){
		state = "init";
		
		if (nextNumber.hasNext()){
			String next = nextNumber.next();
			log.info("outboundcallhandler dialnext:" + next);
			dial(next);
		} else {
			log.warn("outboundCallHandler no next number.");
		}
	}
	
	private void dial(String num){
		String sofia = null;
		String gateway = config.getProperty(IVRConstants.SIP_GATEWAY);
		if (num.startsWith(IPoint.SIP_PREFIX)){
			String nb = point.getNumber().substring(1);
			sofia = "user/" + nb + "@" + service.getWebsite();
		} else {
			String nb = point.getNumber();
			sofia = "sofia/gateway/" + gateway + "/" + nb;
		}
		Integer timeout = point.getTimeout();
		String outboundPlan = config.getProperty(IVRConstants.OUTBOUND_PLAN);
		String data = "{origination_uuid=" 
            + outbound.getChannelId()
            + ",return_ring_ready=true,fail_on_single_reject=true,ignore_early_media=true,origination_caller_id_number="
            + callerId
            + ",originate_timeout=" + timeout
            + "}"
            + sofia
            + " " + outboundPlan;
		log.info("originate " + data);
		Command cmd = new Command(Command.BGAPI, "originate", data);
		CommandResponse response = pipeline.getEngine().runCommand(0, cmd);
		response.setCommandCallback(new ICommandCallback<CommandResponse>(){
			public void onCompleted(CommandResponse response){
				if (!response.isOk()){
					dialNext();
				}
			}
		});
	}
	
	
	public void onEventChannelHangup(Channel channel, Event event){
		if (channel == outbound){
			String cause = event.getEventHeader(EventHeaderNames.HANGUP_CAUSE);
			if (cause != null) {
				log.warn("outboundCallHandler hangup cause:" + cause);
			}
			dialNext();
		}
	}
	
	public void onEventChannelAnswer(Channel channel, Event event){
		String prompt = point.getWav();
		if (prompt != null && prompt.length() > 0) {
			state = "whisper";
			prompt = config.getProperty(IVRConstants.WAV_ROOT) + "/" + prompt;
			channel.executeApplication(0, "playback", prompt);
		} else {
			channel.uuidCommand(0, "uuid_bridge", ch.getChannelId());
		}
	}
	
	
	
	@Override
	public void onEventChannelExecuteComplete(Channel channel, Event event) {
		String application = event.getEventHeader(EventHeaderNames.APPLICATION);
		if ("playback".equals(application)){
			String applicationResponse = event.getEventHeader(EventHeaderNames.APPLICATION_RESPONSE);
			String applicationData = event.getEventHeader(EventHeaderNames.APPLICATION_DATA);
			if (EventHeaderNames.PLAYBACK_RESPONSE_FILE_PLAYED.equals(applicationResponse)){
				log.info("play message {} successfully!", applicationData);
			} else {
				log.warn("play message {} failed! reason {}", applicationData, applicationResponse);
				String prompt = config.getProperty(IVRConstants.NULL_PROMPT);
				channel.executeApplication(0, "playback", prompt);
			}
		}
		
	}
	
	
	
	@Override
	public void onEventPlaybackStop(Channel channel, Event event) {
		if ("whisper".equals(state)){
			channel.uuidCommand(0, "uuid_bridge", ch.getChannelId());
		}
	}
	
	
	
	
}

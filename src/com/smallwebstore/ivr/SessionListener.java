package com.smallwebstore.ivr;

import org.freeswitch.esl.domain.Event;
import org.freeswitch.esl.io.Channel;

public interface SessionListener {
    void onEventChannelAnswer(Channel channel, Event event);
    void onEventChannelApplication(Channel channel, Event event);
    void onEventChannelBridge(Channel channel, Event event);
    void onEventChannelCallState(Channel channel, Event event);
    void onEventChannelCreate(Channel channel, Event event);
    void onEventChannelData(Channel channel, Event event);
    void onEventChannelDestroy(Channel channel, Event event);
    void onEventChannelExecute(Channel channel, Event event);
    void onEventChannelExecuteComplete(Channel channel, Event event);
    void onEventChannelHangup(Channel channel, Event event);
    void onEventChannelHangupComplete(Channel channel, Event event);
    void onEventChannelHold(Channel channel, Event event);
    void onEventChannelOriginate(Channel channel, Event event);
    void onEventChannelOutgoing(Channel channel, Event event);
    void onEventChannelPark(Channel channel, Event event);
    void onEventChannelProgress(Channel channel, Event event);
    void onEventChannelProgressMedia(Channel channel, Event event);
    void onEventChannelState(Channel channel, Event event);
    void onEventChannelUnBridge(Channel channel, Event event);
    void onEventChannelUnHold(Channel channel, Event event);
    void onEventChannelUnPark(Channel channel, Event event);
    void onEventChannelUUID(Channel channel, Event event);
    void onEventCUSTOM(Channel channel, Event event);
    void onEventDTMF(Channel channel, Event event);
    void onEventPlaybackStart(Channel channel, Event event);
    void onEventPlaybackStop(Channel channel, Event event);
    void onEventPrivateCommand(Channel channel, Event event);
    void onEventRecordStart(Channel channel, Event event);
    void onEventRecordStop(Channel channel, Event event);
    
}

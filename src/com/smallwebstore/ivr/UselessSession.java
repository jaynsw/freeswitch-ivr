package com.smallwebstore.ivr;

import java.io.Writer;

import org.freeswitch.esl.domain.Event;
import org.freeswitch.esl.io.Channel;
import org.freeswitch.esl.io.IEngine;
import org.freeswitch.esl.io.SimpleSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UselessSession extends SimpleSession {

		private static final Logger log = LoggerFactory.getLogger(UselessSession.class);

		
		private IConfig config;

		private String state = null;

		
		private String sessionId;
		
		private Writer writer;
		
		private Event channelCreated;
		
		private Channel channel;

		public String getSessionId() {
			return sessionId;
		}

		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}

		public UselessSession(IEngine engine, IConfig config, Channel channel, Event event) {
			super(engine);
			
			this.config = config;
			this.channelCreated = event;
			this.channel = channel;
			
		}

		@Override
		public void onEvent(Channel ch, Event event) {

			log.info("---Session onEvent channl {} event {}",
					ch.getChannelId(), event.getEventName());
			
			
		}
		
		

		

		@Override
		public void setLogger(Writer out) {
			this.writer = out;
		}

		@Override
		public Writer getLogger() {
			return writer;
		}

	}


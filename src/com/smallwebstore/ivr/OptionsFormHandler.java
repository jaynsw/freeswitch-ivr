package com.smallwebstore.ivr;

import java.util.TimeZone;

import org.freeswitch.esl.domain.Event;
import org.freeswitch.esl.domain.EventHeaderNames;
import org.freeswitch.esl.io.Channel;
import org.freeswitch.esl.io.SimpleSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.smallwebstore.dao.IDAOFactory;
import com.smallwebstore.ivr.domain.IEndPoint;
import com.smallwebstore.ivr.domain.IForm;
import com.smallwebstore.ivr.domain.IPoint;
import com.smallwebstore.ivr.domain.IService;
import com.smallwebstore.ivr.domain.ITimeEndPoint;
import com.smallwebstore.ivr.service.IEndPointDAO;
import com.smallwebstore.ivr.service.ITimeEndPointDAO;


public class OptionsFormHandler extends SimpleSession implements IVRHandler{

	private static final Logger log = LoggerFactory.getLogger(OptionsFormHandler.class);
	 
	
	private String state = null;
	
	private IForm form = null;
	private IService service = null;
	private IConfig config = null;
	
	private String dtmf = "";
	
	private int nomatch = 0;
	private int noinput = 0;
	
	private IPoint terminationPoint = null;
	
	private Channel ch;
	
	private Pipeline pipeline;
	
	public OptionsFormHandler(Pipeline pipeline, IForm form){
		super(pipeline.getEngine());
		this.pipeline = pipeline;
		this.service = pipeline.getService();
		this.form = form;
		this.ch = pipeline.getChannel();
		this.config = pipeline.getConfig();
	}
	
	
	
	public void run(){
		state = "dtmf";
		String prompt = form.getPrompt();
		ch.executeApplication(0, "playback", prompt);
	}
	
	private void onDTMF(Channel channel, Event event) {
		String website = service.getWebsite();
		String accountCode = service.getAccountCode();
		IDAOFactory daoFactory = config.getDAOFactory();
    	String datasource = config.getDataSource();
		
    	
		
		String timezone = service.getTimeZone();
		
		IEndPointDAO ptdao = daoFactory.newIntance(IEndPointDAO.class, datasource);
		IEndPoint ep = null;
		ITimeEndPointDAO tptdao = daoFactory.newIntance(ITimeEndPointDAO.class, datasource);
		
		IEndPoint ppt = ptdao.getByDTMF(form.getCode(), dtmf);
		if(ppt == null){
			onNoMatch(channel, event);
			return;
		}
		
		ITimeEndPoint tppt = tptdao.getByEndPointCodeAndTime(ppt.getCode(), TimeZone.getTimeZone(timezone), System.currentTimeMillis());
		if (tppt == null) {
			terminationPoint = tppt;
		} else {
			terminationPoint = ppt;
		}
		
		
		String prompt = form.getTransferPrompt();
		if (prompt != null){
			state = "next";
			channel.executeApplication(0, "playback", prompt);
			
		} else {
			pipeline.next(terminationPoint);
		}
		
	}
	
	public void onEventChannelApplication(Channel channel, Event event) {
		super.onEventChannelApplication(channel, event);
		String subClass = event.getEventHeader(EventHeaderNames.EVENT_SUBCLASS);
		if ("DTMF-TIMEOUT".equals(subClass)){
			int len = dtmf.length();
			if (len == 0){
				onNoInput(channel, event);
			} else if (len <= form.getMaxDigits() && len >= form.getMinDigits()){
				onDTMF(channel, event);
			} else {
				onNoMatch(channel, event);
			}
		}
				
	}
	
	@Override
	public void onEventChannelExecuteComplete(Channel channel, Event event) {
		String application = event.getEventHeader(EventHeaderNames.APPLICATION);
		if ("playback".equals(application)){
			String applicationResponse = event.getEventHeader(EventHeaderNames.APPLICATION_RESPONSE);
			String applicationData = event.getEventHeader(EventHeaderNames.APPLICATION_DATA);
			if (EventHeaderNames.PLAYBACK_RESPONSE_FILE_PLAYED.equals(applicationResponse)){
				log.info("play message {} successfully!", applicationData);
			} else {
				log.warn("play message {} failed! reason {}", applicationData, applicationResponse);
				String prompt = config.getProperty(IVRConstants.NULL_PROMPT);
				channel.executeApplication(0, "playback", prompt);
			}
		}
		
	}
	
	
	
	@Override
	public void onEventPlaybackStop(Channel channel, Event event) {
		if ("dtmf".equals(state)){
			log.info("play message stopped. start to caculate timeout.");
			channel.delayEvent(form.getTimeout() * 1000, "DTMF-TIMEOUT");
		} 
		
		if ("transfer_message".equals(state)){
			pipeline.next(terminationPoint);
		}
	}
	
	
	@Override
	public void onEventDTMF(Channel channel, Event event) {
		String val = event.getEventHeader(EventHeaderNames.DTMF_DIGIT);
		dtmf += val;
		if (dtmf.length() == form.getMaxDigits()){
			onDTMF(channel, event);
		}
	}
	
	@Override
	public void onEventChannelHangup(Channel channel, Event event) {
		if ("hangup".equals(state)){
			log.info("hangup start");
		}
	}
	
	private void onNoInput(Channel channel, Event event){
		dtmf = "";
		noinput++;
		if (noinput > form.getAttempts()){
			//progress timeout
			channel.hangup(0, 607);
		} else {
			String prompt = form.getNoInputPrompt();
			if (prompt == null){
				prompt = form.getPrompt();
			}
			
			channel.executeApplication(0, "playback", prompt);
		}
	}
	private void onNoMatch(Channel channel, Event event){
		dtmf = "";
		nomatch++;
		if (nomatch > form.getAttempts()){
			//invalid number format
			channel.hangup(0, 28);
		} else {
			String prompt = form.getNoMatchPrompt();
			if (prompt == null){
				prompt = form.getPrompt();
			}
			
			channel.executeApplication(0, "playback", prompt);
		}
	}
	
}

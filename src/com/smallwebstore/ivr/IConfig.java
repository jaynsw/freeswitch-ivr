package com.smallwebstore.ivr;

import java.util.Properties;

import com.smallwebstore.dao.IDAOFactory;

public interface IConfig {

	IDAOFactory getDAOFactory();
	String getDataSource();
	String getProperty(String key);
	Properties getProperties();
}

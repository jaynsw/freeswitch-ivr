package com.smallwebstore.ivr;

import java.io.InputStream;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import org.apache.log4j.PropertyConfigurator;
import org.freeswitch.esl.domain.ActionResponse;
import org.freeswitch.esl.io.IEngine;
import org.freeswitch.esl.io.SimpleEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
	
	 private static final Logger logger = LoggerFactory.getLogger(Main.class);
	 
	 public static final String FS_HOST  = "localhost";
	    public static final int FS_PORT     = 8021;
	    public static final String FS_PASS  = "ClueCon";
	    // caller number for outgoing channels
	    public static final String FS_FROM  = "+4921173062190950";
	    // gateway to make outgoing calls
	    public static final String FS_GATEWAY = "sofia/gateway/duro";
	
	public static void main(String[] args) throws ConnectException, InterruptedException{
		InputStream in = logger.getClass().getClassLoader().getResourceAsStream("log4j.properties");
		PropertyConfigurator.configure(in);
		
		Config conf = new Config();
		SimpleSessionFactory factory = new SimpleSessionFactory(conf);
		logger.info("startup, trying to connect to {}:{}...", FS_HOST, FS_PORT);
		
		
        SimpleEngine engine = new SimpleEngine(conf.getProperties(), factory);
        
        ActionResponse<IEngine> response = engine.start();
        response.get();
        
        Thread.currentThread().wait();
        	System.out.println("exit...");
        
	}

}

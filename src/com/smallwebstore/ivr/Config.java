package com.smallwebstore.ivr;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.ibatis.io.Resources;

import com.smallwebstore.dao.IDAOFactory;
import com.smallwebstore.dao.mybatis.DAOFactoryWrapper;
import com.smallwebstore.dao.mybatis.MyBatisDAOFactory;
import com.smallwebstore.dao.mybatis.MyBatisSessionFactory;

public class Config implements IConfig{
	private Properties conf;
	IDAOFactory daoFactory = null;
	
	
	public Config(){
		
		try {
			conf = Resources.getResourceAsProperties(this.getClass().getClassLoader(), "ivr.properties");
			InputStream inputStream = Resources.getResourceAsStream("mybatis.xml");
			MyBatisSessionFactory sf = new MyBatisSessionFactory(inputStream);
			Properties props = Resources.getResourceAsProperties("dao.properties");
			daoFactory = new MyBatisDAOFactory(sf);
			
			daoFactory = new DAOFactoryWrapper(daoFactory, props);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	public Properties getProperties(){
		return conf;
	}
	
	
	
	public IDAOFactory getDAOFactory(){
		return daoFactory;
	}
	
	public String getDataSource(){
		return conf.getProperty(IVRConstants.DATASOURCE);
	}
	
	public String getProperty(String key){
		return conf.getProperty(key);
	}
	
	
}
